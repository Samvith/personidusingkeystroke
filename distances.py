#!/usr/bin/python2.7

from scipy.spatial.distance import mahalanobis as mahal

# n norm of a
def norm(a,n):
    if type(a)==type(1) or type(a)==type(0.5):
        a=[a]
    x=0
    for i in range(0,len(a)):
        x = x + pow(abs(a[i]),n)
    return pow(x,1.0/n)

def diff(a,b):
    if type(a)==type(1) or type(a)==type(0.5):
        return a-b
    x=[0] * len(a)
    for i in range(0,len(a)):
        x[i] = a[i]-b[i]
    return x

# use 1 for Manhattan distance and 2 for Euclidian distance

def dist(a,b,n):
    return norm(diff(a,b),n)


def norm_dist(a,b,n):
    x=dist(a,b,n)
    return x/norm(a,n)/norm(b,n)

def norm_mahal(a,b):
    return mahal(a,b)/norm(a,2)/norm(b,2)














