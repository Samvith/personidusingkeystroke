import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager
from sklearn import svm
import csv
import random
import copy
from itertools import compress

FILE = 'data.csv'

rdata = []
with open(FILE, 'r') as f:
  reader = csv.reader(f)
  first = 0
  for row in reader:
    if first:
      rdata.append(row)
    else:
      features = row
      first = 1

peruser = {}
for row in rdata:
  u = row[0]
  if u not in peruser:
    peruser[u] = []
  peruser[u].append(row[3:])

# Convert data into nice format for later processing
mapping = {}
first5data = []
for u in peruser:
  mapping[u] = len(first5data)
  # Choose first 5 observations (could be random also)
  for vector in peruser[u][:5]:
    first5data.append(vector)

def get_fitness_value(chromosome):
  no_of_dimensions = chromosome.count(1)
  NFRR = 0.0
  NFAR = 0.0
  temp = 0
  for user in peruser:
    temp += 1
    # Filtered per user data
    fdata = map(lambda x: list(compress(x, chromosome)), peruser[user])
    # Filtered first 5 data
    ff5data = map(lambda x: list(compress(x, chromosome)), first5data)
    # Exclude the user from first 5 data
    others = ff5data[:mapping[user]] + ff5data[mapping[user] + 5:]
    FR, size1, FA, size2 = runSVM(fdata, others)
    NFRR += FR*1.0/size1
    NFAR += FA*1.0/size2
    if temp == 100:
      break
  # Average accuracy
  NFRR = NFRR/temp
  NFAR = NFAR/temp

  # Fitness value combines no of dimension and accuracy
  return ((1.0/no_of_dimensions + 10.0/NFRR + 10.0/NFAR), NFRR, NFAR)

def runSVM(user, others):
  # Split user -> one part for training and other for testing
  X_train = np.array(user[:200])
  X_test_user = np.array(user[200:])
  X_test_others = np.array(others)
  clf = svm.OneClassSVM(nu=0.2, kernel="rbf", gamma=0.1)
  clf.fit(X_train)
  y_pred_user = clf.predict(X_test_user)
  y_pred_others = clf.predict(X_test_others)
  # False rejection: Legit users reported as intruders
  FR = y_pred_user[y_pred_user == -1].size
  # False acceptance: Intruders reported as legit users. This should be a very low value
  FA = y_pred_others[y_pred_others == 1].size
  #distances = clf.decision_function(X_test)
  return (FR, y_pred_user.size, FA, y_pred_others.size)

class GeneticAlgorithm(object):

  def __init__(self, gacore):
    # Genetics behind the algorithm
    self.core = gacore

  def run(self):
    population = self.core.initial_population()
    while True:
      # Find the fitness value for every chromosome in population
      fits_population = [(self.core.fitness(ch),  ch) for ch in population]
      if self.core.check_stop(fits_population):
        break
      else:
        population = self.next_generation(fits_population)
    return fits_population

  def next_generation(self, fits_population):
    # Generates parents from the population
    pg = self.core.parents_generator(fits_population)
    size = self.core.get_size()
    next_gen = []
    while len(next_gen) < size:
      parents = next(pg)
      cross = random.random() < self.core.probability_crossover()
      children = self.core.crossover(parents) if cross else parents
      for ch in children:
        mutate = random.random() < self.core.probability_mutation()
        next_gen.append(self.core.mutation(ch) if mutate else ch)
    return next_gen[:]


class GACore(object):

  def __init__(self, max_iterations=20, size=30, no_of_features=31,
               prob_crossover=0.6, prob_mutation=0.02):
    # Generations/Iterations
    self.iterations = 0
    self.max_iterations = max_iterations
    # Determines chromosome length
    self.no_of_features = no_of_features
    # Population size
    self.size = size
    self.prob_crossover = prob_crossover
    self.prob_mutation = prob_mutation

  def get_size(self):
    return self.size

  def probability_crossover(self):
    return self.prob_crossover

  def probability_mutation(self):
    return self.prob_mutation

  def initial_population(self):
    return [self.random_chromosome() for i in range(self.size)]

  def random_chromosome(self):
    # Returns a random bit string
    # 1 means feature is selected, 0 means not selected
    return list(np.random.randint(2, size=(self.no_of_features,)))

  def fitness(self, chromosome):
    # Check the fitness value for the feature subset.
    # filter the data according to the feature subset
    fvalue, NFRR, NFAR = get_fitness_value(chromosome)
    return fvalue

  def check_stop(self, fits_population):
    self.iterations += 1
    #if self.iterations % 10 == 0:
    print "Running..."
    return self.iterations >= self.max_iterations

  def parents_generator(self, fits_population):
    while True:
      father = self.random_pick(fits_population)
      mother = self.random_pick(fits_population)
      yield (father, mother)
  
  def random_pick(self, fits_population):
    adamf, adam = fits_population[random.randint(0, len(fits_population) - 1)]
    evef, eve = fits_population[random.randint(0, len(fits_population) - 1)]
    # Select the more fit parent
    return adam if adamf > evef else eve

  def crossover(self, parents):
    father, mother = parents
    index1 = random.randint(1, self.no_of_features - 2)
    index2 = random.randint(1, self.no_of_features - 2)
    if index1 > index2: 
      index1, index2 = index2, index1
    child1 = father[:index1] + mother[index1:index2] + father[index2:]
    child2 = mother[:index1] + father[index1:index2] + mother[index2:]
    return (child1, child2)

  def mutation(self, chromosome):
    index = random.randint(0, self.no_of_features - 1)
    # mutated chromosome
    xchromo = list(chromosome)
    # Toggle the bit
    xchromo[index] ^= 1
    return xchromo



# Run the Genetic Algorithm
fits_population = GeneticAlgorithm(GACore()).run()
print "Best Feature Subset found is - "
best_gene = sorted(fits_population)[-1][1]
print best_gene
no_of_dimensions = best_gene.count(1)
print "No of dimensions: %s" % (no_of_dimensions)

print 
#best_gene = [0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0]
fvalue, NFRR, NFAR = get_fitness_value(best_gene)

print "Results (Feature selection)"
print "False Rejection rate: %s" % NFRR
print "False Acceptance rate: %s" % NFAR

print 

# All features included
fvalue, NFRR, NFAR = get_fitness_value([1] * 31)
print "Results (All features)"
print "False Rejection rate: %s" % NFRR
print "False Acceptance rate: %s" % NFAR
