#!/usr/bin/python


import sys,time
sys.path.append('..')
from keras.models import Sequential
from keras.layers import  Dense, LSTM, embeddings


# load  dataset
# dataset = np.loadtxt("data.csv")   doesn't work with strings (subject)
from getdata import *

max_features = 31
batch_size = 64
epochs=30

model = Sequential()
model.add(embeddings.Embedding(max_features, 128))
model.add(LSTM(128, dropout=0.2, recurrent_dropout=0.2))
model.add(Dense(1, activation='sigmoid'))

model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])





def train(name='s002'):
    np.random.seed(7)
    [rows, points, X, Y] = getdata()
    [trainX, trainZ, testX, testZ] = splitdata(X, Y, name, 0.8)

    # training
    model.fit(trainX, trainZ, batch_size=batch_size,epochs=epochs, validation_data=(testX, testZ))

    #testing
    scores, acc = model.evaluate(testX, testZ,batch_size=batch_size)
    print("\n%s: %f %s %f%%" % ('score', scores*100, 'accuracy', acc*100))




train()








