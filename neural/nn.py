#!/usr/bin/python


import sys,time
sys.path.append('..')
from keras.models import Sequential
from keras.layers import Dense


# load  dataset
# dataset = np.loadtxt("data.csv")   doesn't work with strings (subject)
from getdata import *



# create model
model = Sequential()
model.add(Dense(12, input_dim=31, activation='relu'))
model.add(Dense(8, activation='relu'))
model.add(Dense(1, activation='sigmoid'))

# Compile model
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])




name='s002'
np.random.seed(7)
[rows, points, X, Y] = getdata()
[trainX, trainZ, testX, testZ] = splitdata(X, Y, name, 0.8)


def train(treinX,treinZ):

    model.fit(trainX, trainZ, epochs=2, batch_size=10)

def test(testX,testZ):
    scores = model.evaluate(testX, testZ)
    print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))



if __name__=='__main__':
    train(trainX,trainZ)
    test(testX,testZ)








