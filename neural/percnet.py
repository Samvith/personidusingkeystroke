#!/usr/bin/python2.7

import sys
sys.path.append('..')

from getdata import *


def nonlin(x,mode=0):
# 0,1 for sigmoid and derivative
# 2,3 for ReLU and derivative
        x=np.longdouble(x)
	if(mode==0):
	    return (1/(1+np.exp(-x)))
	if(mode==1):
	    return (x*(1-x))
	if(mode==2):
	    return np.maximum(x,0)
	if(mode==3):
	    return np.int64(x<0)
	return (x*(1-x))

## some initialisations
np.random.seed(6)
[trainX, trainZ, testX, testZ] = procure()
print trainX.shape
k=0.0005
epoch = 30
hidden = 24
if(1 == 2):
    trainX = np.array([[0,0,1],[0,1,1],[1,0,1],[1,1,1]])
                
    trainZ = np.array([[0],[1],[1],[0]])

    epoch = 50000

# randomly initialize our weights with mean 0
syn0 = 2*np.random.random((len(trainX[0]),hidden)) - 1
syn1 = 2*np.random.random((hidden,1)) - 1

trainsize = len(trainX)
mode = 0 #mode+1 is derivative
## end of initialisations


def feed(l0, syn0,syn1):
    l1 = nonlin(np.dot(l0,syn0),mode)
    l2 = nonlin(np.dot(l1,syn1),mode)
    return [l1,l2]

def test(inp,op,syn0,syn1):
    [l1,l2]=feed(inp,syn0,syn1)
    # how much did we miss the target value?
    l2_error = op - l2 #LOSS
    print "Acc:" + str(100-100*np.mean(np.abs(l2_error)))
    return [l1,l2,l2_error]


for j in xrange(epoch):
    #print j
    # Feed forward through layers 0, 1, and 2
    l0 = trainX
    print "Epoch: %02d" %(j,),
    [l1,l2,l2_error]=test(l0,trainZ,syn0,syn1)

        
    # in what direction is the target value?
    # were we really sure? if so, don't change too much.
    l2_delta = k*l2_error*nonlin(l2,mode+1)

    # how much did each l1 value contribute to the l2 error (according to the weights)?
    l1_error = l2_delta.dot(syn1.T)
    
    # in what direction is the target l1?
    # were we really sure? if so, don't change too much.
    l1_delta = k*l1_error * nonlin(l1,mode+1)


    syn1upd= l1.T.dot(l2_delta)
    syn0upd= l0.T.dot(l1_delta)
    for p in syn1upd:
        for q in p:
            if(q!=0):
                #print "OK NON0"
                break
        if(q!=0):
            break

    syn1 += syn1upd
    syn0 += syn0upd


test(testX,testZ,syn0,syn1)


