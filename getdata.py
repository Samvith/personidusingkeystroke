#!/usr/bin/python2.7

import csv
import numpy as np

def getdata(pathtd="../"):
    rows = []
    X = []
    Y = []
    points = dict()
    f = open(pathtd+"data.csv")
    csvr = csv.reader(f)

    csvr.next()

    for row in csvr:
        rows.append(row)

    # shuffle randomly
    np.random.shuffle(rows)

    for row in rows:
        name = row[0]
        session= row[1]
        rep = row[2]
        count = int(session)*int(rep)
        for i in range(3,len(row)):
            row[i]=float(row[i])
        if points.has_key(name) == False:
            points[name] = []
        points[name].append(row[3:])
        X.append(row[3:])
        Y.append(name)
    return np.array([np.array(rows), np.array(points), np.array(X), np.array(Y)])



# Split training and testing data and label 0/1
def splitdata(X, Y, binarise=True, name='s002',r=9.0/10):
    Z = []
    for i in range(0,len(Y)):
        if Y[i] == name and binarise:
            Z.append([1])
        elif binarise:
            Z.append([0])

    pivot = int(len(X)*r)
    trainX = X[:pivot]
    if binarise:
        trainZ = Z[:pivot]
    else:
        trainZ = Y[:pivot]
    testX = X[pivot:len(X)]
    if binarise:
        testZ = Z[pivot:len(Z)]
    else:
        testZ = Y[pivot:len(Y)]
    return np.array([ np.array(trainX), np.array(trainZ), np.array(testX), np.array(testZ)])

def procure(): 
    [rows, points, X, Y] = getdata()
    return splitdata(X, Y, 's002', 8.0/10)  #split data into 8:2 ,bcos 9:1 causes memory error

















