import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager
from sklearn import svm
import csv
import random
import copy
from itertools import compress
from sklearn.ensemble import IsolationForest
import igraph as ig
import copy

FILE = 'data.csv'

# Random number generator
RandomNG = np.random.RandomState(42)
tcount = 0

rdata = []
with open(FILE, 'r') as f:
  reader = csv.reader(f)
  first = 0
  for row in reader:
    if first:
      rdata.append(row)
    else:
      features = row
      first = 1

peruser = {}
for row in rdata:
  u = row[0]
  if u not in peruser:
    peruser[u] = []
  peruser[u].append(map(lambda x: float(x), row[3:]))

# Convert data into nice format for later processing
mapping = {}
first5data = []
for u in peruser:
  mapping[u] = len(first5data)
  # Choose first 5 observations (could be random also)
  for vector in peruser[u][:5]:
    first5data.append(vector)

def c_value(n):
  return 2.0 * (np.log(n - 1.0) + 0.5772156649) - (2.0 * (n - 1.0)/(n * 1.0))

# Node part of the isolation tree
class Node(object):
  
  def __init__(self, data, node_id, feature, threshold, left, right, leaf):
    # Leaf node
    self.leaf = leaf
    self._id = node_id
    # Dataset of the current node
    self.data = data
    self.size = len(data)
    # Left node
    self.left = left
    # Right node
    self.right = right
    # Feature index in feature list
    self.feature = feature
    # Threshold value for feature list
    self.threshold = threshold

  def set_id(self, value):
    self._id = value
    
  def get_id(self):
    return self._id

#####################
# Isolation Forest
class iForest(object):
  
  def __init__(self, data, no_of_trees=10, sample_size=None):
    self.data = data
    self.no_of_trees = no_of_trees

    if sample_size is None:
      self.sample_size = len(data)
    else:
      self.sample_size = sample_size

    self.c_value = c_value(self.sample_size)

    self.ensemble = []
    for i in xrange(0, no_of_trees):
      self.ensemble.append(iTree(data))
      
  def anomaly_scores(self, data):
    # Find the scores for each sample in dataset
    scores = []
    for sample in data:
      path_len = 0
      for tree in self.ensemble:
        path_len += tree.find_path(sample)[1]
      EH = (1.0 * path_len) / self.no_of_trees
      score = 2.0 ** (-EH / self.c_value)
      scores.append(score)
    return scores

#####################
# Isolation Tree
class iTree(object):

  def __init__(self, data, depth_limit=None):
    self.data = data
    # Tree height
    self.height = 0
    # Increase the global tree count
    global tcount
    tcount += 1
    self.pre = str(tcount) + '_'
    # Max height of any node in tree
    if depth_limit is None:
      self.depth_limit = int(np.ceil(np.log2(len(data))))
    else:
      self.depth_limit = depth_limit
  
    # IG graph version of this tree
    self.graph = ig.Graph()

    self.node_ids = set()
    # Build the tree
    self.root = self.construct_tree(self.data, self.pre + '1', self.height)

  def construct_tree(self, data, node_id, height):
    self.height = max(self.height, height)
    print "Added vertex %s" % (node_id)
    self.graph.add_vertex(node_id)
    int_id = int(node_id.split('_')[-1])
    #print "Int id ", int_id
    if height >= self.depth_limit or len(data) <= 1:
      return Node(data, node_id, None, None, None, None, True)
    else:
      # Choose a random index 
      feature = random.randint(0, 30)
      # Find lower and upper bounds of feature
      #print "Data: ", data
      #print "Feature: %s" % feature
      #print len(data)
      #print len(data)
      #print feature
      lower = np.array(data)[:, feature].min()
      upper = np.array(data)[:, feature].max()
      threshold = random.uniform(lower, upper)

      left_data = filter(lambda x: x[feature] <= threshold, data)
      #print "Going ", self.pre + str(int_id * 2)
      left_node = self.construct_tree(left_data, self.pre + str(int_id * 2), height + 1)
      #print "At node_id %s" % (node_id, self.pre + str(int_id * 2))
      self.graph.add_edge(node_id, self.pre + str(int_id * 2))

      right_data = filter(lambda x: x[feature] > threshold, data)
      right_node = self.construct_tree(right_data, self.pre + str(int_id * 2 + 1), height + 1)
      self.graph.add_edge(node_id, self.pre + str(int_id * 2 + 1))

      return Node(data, node_id, feature, threshold, left_node, right_node, False)

  def find_path(self, sample):
    self.path = []
    self.pathlen = 0
    self.traverse(self.root, sample)
    return (self.path, self.pathlen)

  def traverse(self, node, sample):
    #print "Sample length ", len(sample)
    if node.leaf == True:
      # Leaf node
      if node.size != 1:
        self.pathlen += c_value(node.size)
    else:
      # Internal Node
      self.pathlen += 1
      #print sample
      #print "Index: ", node.feature
      if sample[node.feature] <= node.threshold:
        self.path.append((node.get_id(), node.left.get_id()))
        self.traverse(node.left, sample)
      else:
        self.path.append((node.get_id(), node.right.get_id()))
        self.traverse(node.right, sample)
    
  def get_graph(self):
    # Return an ig graph version of this tree
    return self.graph    
    
# Generate a tree
# Pick a random user 
user = random.choice(peruser.keys())
#user = 's003'
# Exclude the user from first 5 data
others = first5data[:mapping[user]] + first5data[mapping[user] + 5:]
# Add outliers
outliers = [random.choice(others), random.choice(others), random.choice(others)]
root = iTree(peruser[user] + outliers)
g = root.get_graph()

styling = {}
styling["vertex_size"] = [2.5] * g.vcount()
styling["vertex_color"] = ['black'] * g.vcount()
styling["vertex_label_dist"] = 2
styling["bbox"] = (400, 400)
styling["edge_color"] = [(0,0.,0.)] * g.ecount()
styling["edge_width"] = [0.4] * g.ecount()
styling["layout"] = g.layout_reingold_tilford(root=[0])
styling["edge_curved"] = 0.00
styling["margin"] = 10

for o in outliers:
  outlier_path = root.find_path(o)[0]
  eids = g.get_eids(outlier_path)
  print eids

# Display normal tree
ig.plot(g, **styling)

out_styling = copy.deepcopy(styling)
for o in outliers:
  outlier_path = root.find_path(o)[0]
  eids = g.get_eids(outlier_path)
  for e in eids:
    out_styling["edge_color"][e] = 'blue'
    out_styling["edge_width"][e] = 1.9
# Show with outlier highlighted
ig.plot(g, **out_styling)

"""
forest = iForest(peruser[user], 10)
big_graph = ig.Graph()
n_v = [0]
for root in forest.ensemble:
  g1 = root.get_graph()
  big_graph += g1
  n_v.append(g1.vcount())

styling = {}
vstyle={}
vstyle["vertex_size"] = [.01] * big_graph.vcount()
vstyle["vertex_color"]=['black'] * big_graph.vcount()
#vstyle["vertex_label"]=g.vs['name']
vstyle["vertex_label_dist"] = 2
vstyle["bbox"] = (700, 700)
vstyle["edge_color"] = [(0,0.,0.)] * big_graph.ecount()
vstyle["edge_width"] = [0.05] * big_graph.ecount()
vstyle["layout"] = big_graph.layout_reingold_tilford_circular(root=n_v)
vstyle["edge_curved"] = 0.00
vstyle["margin"] = 10
ig.plot(big_graph, **vstyle)

#scores = forest.anomaly_scores(others)
for root in forest.ensemble:
  path, _ = root.find_path(random.choice(others))
  print path
  eids = big_graph.get_eids(path)
  print eids
  out_styling = copy.deepcopy(vstyle)
  for e in eids:
    out_styling["edge_color"][e] = 'red'
    out_styling["edge_width"][e] = 1.0

ig.plot(g, **out_styling)
"""
