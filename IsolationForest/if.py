import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager
from sklearn import svm
import csv
import random
import copy
from itertools import compress
from sklearn.ensemble import IsolationForest

FILE = 'data.csv'

# Random number generator
RandomNG = np.random.RandomState(42)

rdata = []
with open(FILE, 'r') as f:
  reader = csv.reader(f)
  first = 0
  for row in reader:
    if first:
      rdata.append(row)
    else:
      features = row
      first = 1

peruser = {}
for row in rdata:
  u = row[0]
  if u not in peruser:
    peruser[u] = []
  peruser[u].append(row[3:])

# Convert data into nice format for later processing
mapping = {}
first5data = []
for u in peruser:
  mapping[u] = len(first5data)
  # Choose first 5 observations (could be random also)
  for vector in peruser[u][:5]:
    first5data.append(vector)

def runRandomForest(user, others):
  # Split user -> one part for training and other for testing
  X_train = np.array(user[:200])
  X_test_user = np.array(user[200:])
  X_test_others = np.array(others)
  clf = IsolationForest(max_samples=200,
                        contamination=0.18,
                        random_state=RandomNG)
  clf.fit(X_train)
  y_pred_user = clf.predict(X_test_user)
  y_pred_others = clf.predict(X_test_others)
  # False rejection: Legit users reported as intruders
  FR = y_pred_user[y_pred_user == -1].size
  # False acceptance: Intruders reported as legit users. This should be a very low value
  FA = y_pred_others[y_pred_others == 1].size
  #distances = clf.decision_function(X_test)
  return (FR, y_pred_user.size, FA, y_pred_others.size)

NFRR = 0.0
NFAR = 0.0
count_users = len(peruser.keys())
for user in peruser:
  #chromosome = [1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0]
  # No feature selection
  chromosome = [1] * 31
  # Filtered per user data
  fdata = map(lambda x: list(compress(x, chromosome)), peruser[user])
  # Filtered first 5 data
  ff5data = map(lambda x: list(compress(x, chromosome)), first5data)
  # Exclude the user from first 5 data
  others = ff5data[:mapping[user]] + ff5data[mapping[user] + 5:]
  FR, size1, FA, size2 = runRandomForest(fdata, others)
  NFRR += (FR*1.0)  / size1
  NFAR += (FA*1.0) / size2

NFRR = NFRR/count_users
NFAR = NFAR/count_users

print "#" * 10 + " Results " + "#" * 10
print "False Rejection Rate: %s" % NFRR
print "False Acceptance Rate: %s" % NFAR
