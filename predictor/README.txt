The RNN is based heavily on Karparthy's Open Source code.
There are two major dicfferences.

1
	Karparthy's code takes a 1d tensor of text, and predicts next characters.
	This does not conform to the problem statement. It should use a 2d tensor, and predict the 1d tensor data (feature vectors)
	<WORKING>

2
	Parameters of original Karparthy RNN are optimised for realworld plain text.
	There are ~65 chars, including spl chars, there is a frequency distribution of chars, and co-occurences.
	This also does not conform to the problem statement. There is no expected correlation between successive feature vectors.
	Since the problem statement now become more of a continuous value (4 digit decimal) than a discrete value (2 digit integer)
	Many changes had to be made, removing many sparse operations and replacing them with more time consuming dense operations.
	<Working, but very slow>
	The parameters, hidden unit size, learning rate, rnn unroll length, have to be experimented.
	Current values give a very high loss, and the resultant can be easily discriminated from original, by percnet,clustering,svm.
	As an example, it is not able to learn that the list to be predicted is sorted and what the increments are. 



