#!/usr/bin/python2.7


"""
Minimal word-level Vanilla RNN model. Original written by Andrej Karpathy (@karpathy), for character sequence predicton in text
BSD License

This version is a sequence to sequence prediction, i e, predicts rows in a table of 4 digit floats
Part of the project (beyond the requirement) for SMAI course, Spring 2K17
International Institute of Information Technology, Hyderabad

Parameters of the network to be experimented with:
hidden_size = 100 # size of hidden layer of neurons
seq_length = 50 # number of steps to unroll the RNN for
learning_rate = 1e-1


"""
import numpy, sys, re, time, random, csv
sys.path.append('..')
import distances

def itow(index):
  #if index > len(words) or index <0:
    #return -1
  return words[index][1]
def itof(index):
  if index > len(words) or index <0:
    return -1
  return words[index][0]
def wtoi(wd):
  for i in range(0,len(words)):
    if wd == itow(i):
      return i
  #return word_to_ix[wd] # -1


# gradient checking
def gradCheck(inputs, target, hprev):
  global Wxh, Whh, Why, bh, by
  num_checks, delta = 10, 1e-5
  _, dWxh, dWhh, dWhy, dbh, dby, _ = lossFun(inputs, targets, hprev)
  for param,dparam,name in zip([Wxh, Whh, Why, bh, by], [dWxh, dWhh, dWhy, dbh, dby], ['Wxh', 'Whh', 'Why', 'bh', 'by']):
    s0 = dparam.shape
    s1 = param.shape
    assert s0 == s1, 'Error dims dont match: %s and %s.' % (`s0`, `s1`)
    print >> sys.stderr, name
    for i in xrange(num_checks):
      ri = int(random.uniform(0,param.size))
      # evaluate cost at [x + delta] and [x - delta]
      old_val = param.flat[ri]
      param.flat[ri] = old_val + delta
      cg0, _, _, _, _, _, _ = lossFun(inputs, targets, hprev)
      param.flat[ri] = old_val - delta
      cg1, _, _, _, _, _, _ = lossFun(inputs, targets, hprev)
      param.flat[ri] = old_val # reset old value for this parameter
      # fetch both numerical and analytic gradient
      grad_analytic = dparam.flat[ri]
      grad_numerical = (cg0 - cg1) / ( 2 * delta )
      rel_error = abs(grad_analytic - grad_numerical) / abs(grad_numerical + grad_analytic)
      print >> sys.stderr, '%f, %f => %e ' % (grad_numerical, grad_analytic, rel_error)
      # rel_error should be on order of 1e-7 or less


def lossFun(inputs, targets, hprev):
  """
  inputs,targets are both list of integers.
  hprev is Hx1 array of initial hidden state
  returns the loss, gradients on model parameters, and last hidden state
  """
  xs, hs, ys, ps = {}, {}, {}, {}
  hs[-1] = numpy.copy(hprev)
  loss = 0
  # forward pass
  for t in range(0,len(inputs)):
    #xs[t] = numpy.zeros((vocab_size,1)) # encode in 1-of-k representation
    #xs[t][inputs[t]] = 1 #for descrete only
    xs[t]=numpy.array([[distances.dist(i,inputs[t],2)] for i in xrange(vocab_size)]) #euclidian distance
    hs[t] = numpy.tanh(numpy.dot(Wxh, xs[t]) + numpy.dot(Whh, hs[t-1]) + bh) # hidden state
    ys[t] = numpy.dot(Why, hs[t]) + by # unnormalized log probabilities for next words
    ps[t] = numpy.exp(ys[t]) / numpy.sum(numpy.exp(ys[t])) # probabilities for next words
    loss += -numpy.log(ps[t][targets[t],0]) # softmax (cross-entropy loss)
  # backward pass: compute gradients going backwards
  dWxh, dWhh, dWhy = (numpy.zeros_like(Wxh)), (numpy.zeros_like(Whh)), (numpy.zeros_like(Why))
  dbh, dby = (numpy.zeros_like(bh)), (numpy.zeros_like(by))
  dhnext = (numpy.zeros_like(hs[0]))
  for t in reversed(range(0,len(inputs))):
    dy = numpy.copy(ps[t])
    #dy[targets[t]] -= 1 # backprop into y. see http://cs231n.github.io/neural-networks-case-study/#grad if confused here
    dy = numpy.array([dy[i]-distances.dist(i,targets[t],2) for i in xrange(vocab_size)])
    #print xs[t].shape , dby.shape , dy.shape
    dWhy += numpy.dot(dy, hs[t].T)
    dby += dy
    dh = numpy.dot(Why.T, dy) + dhnext # backprop into h
    dhraw = (1 - hs[t] * hs[t]) * dh # backprop through tanh nonlinearity
    dbh += dhraw
    dWxh += numpy.dot(dhraw, xs[t].T)
    dWhh += numpy.dot(dhraw, hs[t-1].T)
    dhnext = numpy.dot(Whh.T, dhraw)
  for dparam in [dWxh, dWhh, dWhy, dbh, dby]:
    numpy.clip(dparam, -5, 5, out=dparam) # clip to mitigate exploding gradients
  return loss, dWxh, dWhh, dWhy, dbh, dby, hs[len(inputs)-1]

def sample(h, seed_ix, n):
  """ 
  sample a sequence of integers from the model 
  h is memory state, seed_ix is seed letter for first time step
  """
  x = numpy.zeros((vocab_size, 1))
  x[seed_ix] = 1  #int slises
  ixes = []
  for t in xrange(n):
    h = numpy.tanh(numpy.dot(Wxh, x) + numpy.dot(Whh, h) + bh)
    y = numpy.dot(Why, h) + by
    p = numpy.exp(y) / numpy.sum(numpy.exp(y))
    probg = p.ravel()/1.0/p.ravel().sum()
    #ix = numpy.random.choice(range(vocab_size), p=probg)
    rn = random.random()
    cnt=0.0
    for i in range(len(probg)):
      cnt+=p[i]
      if rn <= cnt:
        ix=i
        break
    x = numpy.zeros((vocab_size, 1))
    x[ix] = 1
    ixes.append(ix)
  return ixes


def watch():
  global start_time
  end_time=time.time()
  old_st=start_time
  start_time=end_time
  return end_time - old_st

def startmain():
  global data, words, datasize, vocab_size, word_to_ix, ix_to_word, ix_to_freq, printsig, vocab_precession
  global hidden_size, seq_length, learning_rate, nfeat
  global Wxh, Whh, Why, bh, by, n, p, mWxh, mWhh, mWhy, mbh, mby, smooth_loss,loss, dWxh, dWhh,dWhy,dbh,dby
  global hprev
  global start_time, end_time
  vocab_precession=10000; nfeat=31 #not negotiable::prpperty of the data
  start_time=time.time()
  data=[]
  with open("../data.csv","rb") as source:
    rdr= csv.reader( source )
    for r in rdr:
      if r[0]=='s002':
        data.append([int(float(e)*vocab_precession) for e in r[3:]])

  # hyperparameters, original Karparthy
  #printsig=100 # print loss every printsigth time
  ##hidden_size = 100 # size of hidden layer of neurons
  ##seq_length = 25 # number of steps to unroll the RNN for
  ##learning_rate = 1e-1

  printsig=1 # print loss every printsigth time
  hidden_size = 100 # size of hidden layer of neurons
  seq_length = 5 # number of steps to unroll the RNN for
  learning_rate = 1e-1
  print 'hidden unit size %d, rnn unroll steps %d, learning rate %d' %(hidden_size,seq_length,learning_rate)
  #data = open('result.s002.csv', 'r').read() # should be simple plain text file
  #data=data.lower()
  #data=numpy.array(data)
  wrdt = numpy.ndarray.flatten(numpy.array(data))
  #words = list(set(data))
  words = [wrdt.min(),wrdt.max()]
  #wct={}
  #for word in data:
    #wct[word]=0
  #for word in data:
    #wct[word]+=1
  #word_to_rank=[ [wct[word],word] for word in words]
  #word_to_rank.sort()
  #words=word_to_rank
  data_size, vocab_size = len(wrdt), words[1]-words[0]
  #word_to_ix = { ch[1]:i for i,ch in enumerate(words) };print 'time', watch(), 153
  #ix_to_word = { i:ch[1] for i,ch in enumerate(words) }
  #ix_to_freq = { i:ch[0] for i,ch in enumerate(words) }

  # model parameters
  Wxh = (numpy.random.randn(hidden_size, vocab_size)*0.01) # input to hidden
  Whh = (numpy.random.randn(hidden_size, hidden_size)*0.01) # hidden to hidden
  Why = (numpy.random.randn(vocab_size, hidden_size)*0.01) # hidden to output
  bh = numpy.zeros((hidden_size, 1)) # hidden bias
  by = numpy.zeros((vocab_size, 1)) # output bias
  n, p = 0, 0
  mWxh, mWhh, mWhy = numpy.zeros_like(Wxh), numpy.zeros_like(Whh), numpy.zeros_like(Why)
  mbh, mby = numpy.zeros_like(bh), (numpy.zeros_like(by)) # memory variables for Adagrad
  smooth_loss = -numpy.log(1.0/vocab_size)*seq_length # loss at iteration 0
  print >> sys.stderr, 'loading time', watch()

def loop():
    global data, words, datasize, vocab_size, word_to_ix, ix_to_word, ix_to_freq, printsig, vocab_precession
    global hidden_size, seq_length, learning_rate, nfeat
    global Wxh, Whh, Why, bh, by, n, p, mWxh, mWhh, mWhy, mbh, mby, smooth_loss,loss, dWxh, dWhh,dWhy,dbh,dby
    global hprev
    global start_time, end_time
    # prepare inputs (we're sweeping from left to right in steps seq_length long)
    if p+seq_length+1 >= len(data) or n == 0: 
      hprev = numpy.zeros((hidden_size,1)) # reset RNN memory
      p = 0 # go from start of data
    inputs =  data[p  ][0:seq_length]
    targets = data[p+1][0:seq_length]

    # sample from the model now and then
    if n % printsig == 0:
      sample_ix = sample(hprev, inputs[0], nfeat)
      txt = []
      for ix in sample_ix:
        txt.append(ix)

      if 1==1:
          pass
      if 2==2:
         if 3==3:
             pass
         else:
             pass

      print 'oput----\n %s \noput----\n' % ([i*1.0/vocab_precession for i in txt], )
      print >> sys.stderr, 'work took %f time '  % (watch(),)

    # forward seq_length words through the net and fetch gradient
    loss, dWxh, dWhh, dWhy, dbh, dby, hprev = lossFun(inputs, targets, hprev)
    smooth_loss = smooth_loss * 0.999 + loss * 0.001
    if n % printsig == 0:
      print >> sys.stderr, 'iter %d, loss: %f' % (n, smooth_loss) # print progress
      print >> sys.stdout, 'iter %d, loss: %f' % (n, smooth_loss) # print progress
  
    # perform parameter update with Adagrad
    for param, dparam, mem in zip([Wxh, Whh, Why, bh, by], 
                                [dWxh, dWhh, dWhy, dbh, dby], 
                                [mWxh, mWhh, mWhy, mbh, mby]):
      mem += dparam * dparam
      param += -learning_rate * dparam / numpy.sqrt(mem + 1e-8) # adagrad update

    p += seq_length # move data pointer
    n += 1 # iteration counter
    return txt

if __name__ == '__main__':
  startmain()
  while True:
    loop()



