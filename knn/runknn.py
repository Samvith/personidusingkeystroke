#!/usr/bin/python2.7

from knn import *


#k = raw_input('Enter value of k to get accuracy percentage of k-NN algorithm: ')
#distmetric = raw_input('Enter Distance metric: ')

if(len(sys.argv)!=3):
	print >> sys.stderr , bcolor.WARNING , "use python file kvalue distancemetric" , bcolor.ENDC
	exit(1)
k=sys.argv[1]
distmetric=sys.argv[2]
print >> sys.stderr , bcolor.WARNING , "Starting %s %s %s" % ( sys.argv[0] , k , distmetric ) , bcolor.ENDC

print str(accuracy(int(k),int(distmetric))) + '%' + ' cases returned correct results'


