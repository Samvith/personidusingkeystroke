import plotly
from plotly.graph_objs import Scatter, Layout

plotly.offline.plot({
    "data": [Scatter(x=['k=1', 'k=5', 'k=10', 'k=50', 'k=100', 'k=200', 'k=300', 'k=500', 'k=1000'], y=[76, 74, 74, 68, 65, 60, 54, 50, 43])],
    "layout": Layout(title="k-NN Percentage Accuracy (Manhattan)")
}, filename='manhattan-accuracy.html', image='jpeg')


plotly.offline.plot({
    "data": [Scatter(x=['k=1', 'k=5', 'k=10', 'k=50', 'k=100', 'k=200', 'k=300', 'k=500', 'k=1000'], y=[75, 75, 74, 69, 65, 62, 57, 50, 42])],
    "layout": Layout(title="k-NN Percentage Accuracy (Euclidean)")
}, filename='euclidean-accuracy.html', image='jpeg')
