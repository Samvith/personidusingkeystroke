#!/bin/bash

#Manhattan
time ./runknn.py 1 0 >> report.txt
time ./runknn.py 5 0 >> report.txt
time ./runknn.py 10 0 >> report.txt
time ./runknn.py 50 0 >> report.txt
time ./runknn.py 100 0 >> report.txt
time ./runknn.py 200 0 >> report.txt
time ./runknn.py 300 0 >> report.txt
time ./runknn.py 500 0 >> report.txt
time ./runknn.py 1000 0 >> report.txt

#Euclidean
time ./runknn.py 1 2 >> report.txt
time ./runknn.py 5 2 >> report.txt
time ./runknn.py 10 2 >> report.txt
time ./runknn.py 50 2 >> report.txt
time ./runknn.py 100 2 >> report.txt
time ./runknn.py 200 2 >> report.txt
time ./runknn.py 300 2 >> report.txt
time ./runknn.py 500 2 >> report.txt
time ./runknn.py 1000 2 >> report.txt
