#!/usr/bin/python2.7

#getting and splitting the data
import math, sys, os, csv
import numpy as np
sys.path.append("..")
from distances import *
from printclr import *
from getdata import *


# print "Testing getdata()"
get = getdata("../")

# print "Testing splitdata()"
split = splitdata(get[2], get[3],r=9.0/10,binarise=False)
train_length = len(split[0])
test_length = len(split[2])
#Find euclidean distance from two given points
def euclidean(data1, data2):
	points = zip(data1, data2)
	diffs_sqrd_dist = [pow(a-b, 2) for (a,b) in points]
	return math.sqrt(sum(diffs_sqrd_dist))

#Generate sorted distance array using splitted data and a given index
def dist_iterator(test_index, metric):
	trainX = split[0]
	trainY = split[1]
	test_point = split[2][test_index]
	test_point_label = split[3][test_index]

	distance_set = []
	for i in range(len(trainX)):
		train_point = trainX[i]
		train_point_name = trainY[i]
		if metric == 0: # manhattan
			distval= dist(train_point, test_point, 1)
#		if metric == 1: # normalised manhattan
#			distval= norm_dist(train_point, test_point, 1)
		if metric == 2: # euclidian
			distval= dist(train_point, test_point, 2)
#		if metric == 3: # normalised euclidian
#			distval= norm_dist(train_point, test_point, 2)
		distance_set.append( (distval, train_point_name) )
	
	dtype = [('distance', float), ('point_name', 'S10')]
	dtype_distance_set = np.array(distance_set, dtype=dtype)

	sorted_distance_set = np.sort(dtype_distance_set, order='distance')
	return np.array([sorted_distance_set, test_point_label])

# print dist_iterator(split, 0)

def knn(k, sorted_distset_with_testpoint_label):
	#building an array for counting occurences of different labels in sorted neighbors list
	neighbor_count = []
	tuple_neighbor_count = []
	for i in range(57):
		neighbor_count.append([i+1, 0])
	# print neighbor_count

	test_point_label = sorted_distset_with_testpoint_label[1]
	# print "Test point - " + test_point_label
	# print "Value of k - " + str(k)
	#increment neighbor count for the top k neighbors
	for i in range(k):
		label_number = int(sorted_distset_with_testpoint_label[0][i][1][2:])
		neighbor_count[label_number-1][1] = neighbor_count[label_number-1][1] + 1
	# print neighbor_count

	#building tuple based nieghbor count list
	for i in range(len(neighbor_count)):
		tuple_neighbor_count.append((neighbor_count[i][0], neighbor_count[i][1]))
	# print tuple_neighbor_count
	
	#sorting the tuple_neighbor_count in descending order of count
	dtype = [('label', int), ('count', int)]
	dtype_neighbor_count = np.array(tuple_neighbor_count, dtype=dtype)
	asc_neighbor_count = np.sort(dtype_neighbor_count, order=['count','label'])
	desc_neighbor_count = asc_neighbor_count[::-1]
	# print desc_neighbor_count

	if desc_neighbor_count[0][0] != desc_neighbor_count[1][0]:
		# print "Test point - " + test_point_label
		# print "KNN result point - " + 's0' + str(desc_neighbor_count[0][0])
		if desc_neighbor_count[0][0] == int(test_point_label[2:]):
			return True
		else:
			return False
	else:
		# print "Test point - " + test_point_label
		# print "KNN result point - " + 's0' + str(desc_neighbor_count[0][0]) + 'and' + 's0' + str(desc_neighbor_count[1][0])
		return False

def accuracy(k, distmetric):
	true_count = 0
	for i in range(test_length):
		print >> sys.stderr , bcolor.OKBLUE , "Test Case %04d:" % i, bcolor.ENDC,
		if knn(k, dist_iterator(i, distmetric)) == True:
			true_count = true_count + 1
			print >> sys.stderr , bcolor.OKGREEN , "    PASSED" , bcolor.ENDC
		else:
			print >> sys.stderr , bcolor.FAIL , "    FAILED" , bcolor.ENDC
	accuracy_percent = (true_count * 100) / test_length
	return accuracy_percent
