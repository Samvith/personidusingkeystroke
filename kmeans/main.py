import sys, csv, random, pickle
import numpy as np
from math import sqrt
from scipy.spatial.distance import mahalanobis as mahal

TEST_DATA_SIZE = 5100
ITERATIONS = 10
SUB_CLASSES = 3
CLASSES = 51 * SUB_CLASSES
INFINITY = float("inf")

# -----------------------------------------------------------------------------
def compute_distance(p1, p2):
    f1 = p1.get_feature_vector()
    f2 = p2.get_feature_vector()
    #return mahal(f1, f2, np.float128(np.cov(zip(f1, f2))))
    d = 0
    for i in xrange(len(f1)):
        d += abs((f1[i] - f2[i]) * 1.0)
    return d

# -----------------------------------------------------------------------------
class DataPoint:

    # -------------------------------------------------------------------------
    def __init__(self, vector):
        self.feature_vector = vector[3:]
        self.subject = vector[0]

    # -------------------------------------------------------------------------
    def get_subject(self):
        return self.subject

    # -------------------------------------------------------------------------
    def get_feature_vector(self):
        return self.feature_vector

# -----------------------------------------------------------------------------
class Cluster:

    # -------------------------------------------------------------------------
    def __init__(self, points):
        # List of DataPoint objects
        self.points = points
        self.contribution = None

    # -------------------------------------------------------------------------
    def get_centroid(self):
        fvectors = [x.get_feature_vector() for x in self.points]
        centroid_vector = [sum(i) / len(self.points) for i in zip(*fvectors)]
        return DataPoint(["Something"] * 3 + centroid_vector)

    # -------------------------------------------------------------------------
    def add_point(self, p):
        self.points.append(p)

    # -------------------------------------------------------------------------
    def get_points(self):
        return self.points

    # -------------------------------------------------------------------------
    def flush_points(self):
        self.points = []

    # -------------------------------------------------------------------------
    def set_contribution(self, contribution):
        self.contribution = contribution

# -----------------------------------------------------------------------------
def get_subject(cluster):
    points = cluster.get_points()
    count_dict = {}
    for point in points:
        this_class = point.get_subject()
        if count_dict.has_key(this_class):
            count_dict[this_class] += 1
        else:
            count_dict[this_class] = 1
    tmp = sorted(count_dict.items(), key=lambda x: x[1], reverse=True)
    return tmp[0][0]

# -----------------------------------------------------------------------------
def kmeans(k, iterations, training):
    centroids = [] #training[:k]
    tmpset = {}
    for point in training:
        if len(centroids) == k:
            break
        subj = point.get_subject()
        if tmpset.has_key(subj):
            tmpset[subj] += 1
            if tmpset[subj] <= SUB_CLASSES:
                centroids.append(point)
        else:
            tmpset[subj] = 1
            centroids.append(point)
    """
    tmpset = set([])
    for point in training:
        if len(centroids) == k:
            break
        subj = point.get_subject()
        if subj in tmpset:
            continue
        else:
            tmpset.add(subj)
            centroids.append(point)
    """
    print len(centroids)
    # print "************************"
    # print len(centroids)
    # print [x.get_subject() for x in centroids]
    # print "************************"
    clusters = [Cluster([]) for x in xrange(k)]

    for iteration in xrange(iterations):
        print "Iteration:", iteration
        for cluster in clusters:
            cluster.flush_points()

        for point in training:
            mindist = INFINITY
            cluster_index = INFINITY

            for i in xrange(len(centroids)):
                d = compute_distance(centroids[i], point)
                if d < mindist:
                    mindist = d
                    cluster_index = i
            if cluster_index == INFINITY:
                # Shouldn't come here
                raise ValueError("Something bad happened")
            clusters[cluster_index].add_point(point)
        print [len(x.points) for x in clusters]
        centroids = [x.get_centroid() for x in clusters]

    return clusters

# -----------------------------------------------------------------------------
def read_input(file_name):
    vectors = []
    with open(file_name, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            floatrow = []
            for i in row:
                try:
                    floatrow.append(float(i))
                except ValueError:
                    floatrow.append(i)
            vectors.append(DataPoint(floatrow))

    vectors = vectors[1:]
    random.shuffle(vectors)
    return vectors

# -----------------------------------------------------------------------------
def split_data(index, dataset):
    test_start = index * TEST_DATA_SIZE
    test_end = test_start + TEST_DATA_SIZE
    testing = dataset[test_start : test_end]
    training = dataset[:test_start] + dataset[test_end:]

    return training, testing

# -----------------------------------------------------------------------------
def classify(clusters, testing):
    labels = []
    correct = 0
    wrong = 0
    for point in testing:
        distances = []
        for cluster in clusters:
            distances.append((compute_distance(point, cluster.get_centroid()),
                              cluster))
        distances.sort(key=lambda x: x[0])
        classified = get_subject(distances[0][1])
        actual = point.get_subject()
        if classified == actual:
            correct += 1
        else:
            wrong += 1
        labels.append(classified)
        print "classified: ", classified
        print "actual:", actual
        print "\n\n\n"
    print correct, wrong
    return labels

# -----------------------------------------------------------------------------
def get_class_counts(cluster):
    data = cluster.get_points()
    count_dict = {}
    for point in data:
        this_class = point.get_subject()
        if count_dict.has_key(this_class):
            count_dict[this_class] += 1
        else:
            count_dict[this_class] = 1

    print "Cluster Data"
    print "***********************************************************************"
    tmp = sorted(count_dict.items(), key=lambda x: x[1], reverse=True)
    maxi = tmp[0][1]
    mean = sum([x[1] for x in tmp]) * 1.0 / len(tmp)
    try:
        mini = tmp[1][1]
    except IndexError:
        mini = -1

    contribution = ""
    if mini == -1:
        contribution = "bad"
    else:
        if maxi > 1.5 * mini and mean < mini / 1.5:
            contribution = "good"
        else:
            contribution = "bad"

    cluster.set_contribution(contribution)
    print tmp
    print maxi, mini, mean
    print "***********************************************************************"
    return tmp[0][0], contribution

# -----------------------------------------------------------------------------
def check_kmeans():
    dataset = []
    dataset.append(DataPoint(['s001', 1, 2, 1, 0]))
    dataset.append(DataPoint(['s001', 1, 2, 1, 1]))
    dataset.append(DataPoint(['s001', 1, 2, 0, 1]))
    dataset.append(DataPoint(['s001', 1, 2, 0, 0]))
    dataset.append(DataPoint(['s001', 1, 2, -1, 0]))
    dataset.append(DataPoint(['s001', 1, 2, -1, -1]))
    dataset.append(DataPoint(['s001', 1, 2, 0, -1]))
    dataset.append(DataPoint(['s001', 1, 2, -1, 1]))

    dataset.append(DataPoint(['s002', 1, 2, 15, 15]))
    dataset.append(DataPoint(['s002', 1, 2, 15, 16]))
    dataset.append(DataPoint(['s002', 1, 2, 17, 16]))
    dataset.append(DataPoint(['s002', 1, 2, 16, 16]))
    dataset.append(DataPoint(['s002', 1, 2, 17, 17]))
    dataset.append(DataPoint(['s002', 1, 2, 14, 14]))

    random.shuffle(dataset)
    clusters = kmeans(2, 10, dataset)
    for cluster in clusters:
        get_class_counts(cluster.get_points())

# -----------------------------------------------------------------------------
if __name__ == "__main__":
    dataset = read_input('dataset.csv')
    # Number of records = 20400
    # Number of subjects (classes) = 51
    # Missing subjects = [1, 6, 9, 14, 23, 45] (from 1 to 57 included)

    #for i in xrange(len(dataset) / TEST_DATA_SIZE):

    good = 0
    bad = 0
    training, testing = split_data(0, dataset)

    if len(sys.argv) > 1 and sys.argv[1] == "pickle":
        clusters = kmeans(CLASSES, ITERATIONS, training)
        for cluster in clusters:
            # Also sets good and bad cluster
            get_class_counts(cluster)

        with open("clusters.pickle", "wb") as f:
            pickle.dump(clusters, f)
        for cluster in clusters:
            if cluster.contribution == "good":
                good += 1
            else:
                bad += 1
        print good, bad
    else:
        with open("clusters.pickle", "rb") as f:
            clusters = pickle.load(f)

        g = []
        b = []
        for cluster in clusters:
            label, c = get_class_counts(cluster)
            if c == "good":
                good += 1
                g.append(label)
            else:
                bad += 1
                b.append(label)
        print good, bad
        g.sort()
        b.sort()
        print len(g), len(b)
        good_clusters = filter(lambda x: x.contribution == "good", clusters)
        classified_labels = classify(good_clusters, testing)

    # classify(clusters, testing)
