import sys, csv, random, pickle
import numpy as np
from math import sqrt
from scipy.spatial.distance import mahalanobis as mahal

TEST_DATA_SIZE = 5100
CLASSES = 51
INFINITY = float("inf")
index_mapping = {'s037': 31, 's055': 48, 's030': 24, 's031': 25, 's048': 41, 's056': 49, 's057': 50, 's054': 47, 's033': 27, 's052': 45, 's053': 46, 's050': 43, 's051': 44, 's018': 13, 's019': 14, 's038': 32, 's039': 33, 's012': 8, 's013': 9, 's010': 6, 's011': 7, 's016': 11, 's017': 12, 's032': 26, 's015': 10, 's005': 3, 's004': 2, 's007': 4, 's003': 1, 's002': 0, 's041': 35, 's040': 34, 's043': 37, 's042': 36, 's008': 5, 's047': 40, 's046': 39, 's049': 42, 's029': 23, 's028': 22, 's027': 21, 's026': 20, 's025': 19, 's024': 18, 's022': 17, 's021': 16, 's020': 15, 's034': 28, 's044': 38, 's035': 29, 's036': 30}

# -----------------------------------------------------------------------------
def compute_distance(p1, p2):
    f1 = p1
    f2 = p2
    def euclidean():
        d = 0
        for i in xrange(len(f1)):
            d += (f1[i] - f2[i]) ** 2
        return sqrt(d)

    def manhattan():
        d = 0
        for i in xrange(len(f1)):
            d += abs((f1[i] - f2[i]) * 1.0)
        return d

    def mahalanobis():
        return mahal(f1, f2, np.float128(np.cov(zip(f1, f2))))

    return manhattan()

# -----------------------------------------------------------------------------
class DataPoint:

    # -------------------------------------------------------------------------
    def __init__(self, vector):
        self.feature_vector = vector[3:]
        self.subject = vector[0]
        self.assoc_vector = []

    # -------------------------------------------------------------------------
    def get_subject(self):
        return self.subject

    # -------------------------------------------------------------------------
    def get_feature_vector(self):
        return self.feature_vector

    # -------------------------------------------------------------------------
    def set_assoc_vector(self, v):
        self.assoc_vector = v

# -----------------------------------------------------------------------------
def fuzzy_kmeans(k, training):

    def add_vector(first, second):
        return [x + y for x, y in zip(first, second)]

    def multiply_vector(v, alpha):
        return [alpha * x for x in v]

    centroids = [] #training[:k]
    centroid_assoc_vectors = []

    tmpset = set([])
    for point in training:
        if len(centroids) == k:
            break
        subj = point.get_subject()
        if subj in tmpset:
            continue
        else:
            tmpset.add(subj)
            centroids.append(point)
            assoc_vector = [0] * k
            assoc_vector[index_mapping[subj]] = 1
            centroid_assoc_vectors.append(assoc_vector)

    # Sort centroids based on class label
    reordered_centroids = [0] * k
    for centroid in centroids:
        tmp = index_mapping[centroid.get_subject()]
        assert(tmp >= 0  and tmp <= 50)
        reordered_centroids[index_mapping[centroid.get_subject()]] = centroid

    centroids = reordered_centroids

    numerator = [0] * 31
    denominator = 0
    new_centroids = [x for x in centroids]
    for point in training:
        centroid_distances = [compute_distance(centroid.get_feature_vector(),
                                               point.get_feature_vector()) for centroid in new_centroids]
        if 0.0 in centroid_distances:
            # The point is one of the randomly chosen initial point
            continue
        # (1/3, 1/5, 1/7) * alpha = 1
        alpha = 1.0 / sum([1.0 / x for x in centroid_distances])
        # (alpha/3, alpha/5, alpha / 7)
        assoc_vector = [alpha * x for x in centroid_distances]
        new_centroids = []
        for centroid in centroids:
            subj = centroid.get_subject()
            # vx[A]
            weight_value = assoc_vector[index_mapping[subj]]
            this_numerator = add_vector(numerator, multiply_vector(point.get_feature_vector(),
                                                                   weight_value))
            this_numerator = add_vector(this_numerator, centroid.get_feature_vector())
            numerator = [x for x in this_numerator]
            this_denominator = 1 + denominator + weight_value
            denominator += weight_value
            new_centroid = DataPoint(["something"] * 3 + \
                                     multiply_vector(this_numerator,
                                     1.0 / this_denominator))
            new_centroids.append(new_centroid)

    return new_centroids

# -----------------------------------------------------------------------------
def read_input(file_name):
    vectors = []
    with open(file_name, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            floatrow = []
            for i in row:
                try:
                    floatrow.append(float(i))
                except ValueError:
                    floatrow.append(i)
            vectors.append(DataPoint(floatrow))

    vectors = vectors[1:]
    random.shuffle(vectors)
    return vectors

# -----------------------------------------------------------------------------
def split_data(index, dataset):
    test_start = index * TEST_DATA_SIZE
    test_end = test_start + TEST_DATA_SIZE
    testing = dataset[test_start : test_end]
    training = dataset[:test_start] + dataset[test_end:]

    return training, testing

# -----------------------------------------------------------------------------
def classify(centroids, testing):
    labels = []
    correct = 0
    wrong = 0
    for point in testing:
        m = INFINITY
        class_index = None
        for i in xrange(len(centroids)):
            d = compute_distance(centroids[i].get_feature_vector(),
                                 point.get_feature_vector())
            if d < m:
                m = d
                class_index = i
        assert(class_index is not None)
        if class_index == index_mapping[point.get_subject()]:
            correct += 1
        else:
            wrong += 1
    print correct, wrong
    return labels

# -----------------------------------------------------------------------------
if __name__ == "__main__":
    dataset = read_input('dataset.csv')
    # Number of records = 20400
    # Number of subjects (classes) = 51
    # Missing subjects = [1, 6, 9, 14, 23, 45] (from 1 to 57 included)

    training, testing = split_data(0, dataset)

    new_centroids = fuzzy_kmeans(CLASSES, training)
    classify(new_centroids, testing)
