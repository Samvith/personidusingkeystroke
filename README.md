# README #

Bitbucket Repository ==> https://bitbucket.org/Samvith/personidusingkeystroke/

# To run the SVM code


```
#!bash

python SVM/genetic-svm.py
```


goodgenes.txt contain some feature subsets that give good results

# To run Isolation forest


```
#!bash

python IsolationForest/if.py
```


To view the construction of an Isolation tree on the dataset, run


```
#!bash

python IsolationForest/generate-tree.py

```
# To run the k-means code


To create a pickle file which contains trained model
```
python main.py pickle
```

To directly import from the already trained pickle `clusters.pickle`
```
python main.py
```

# To run Fuzzy clustering

```
python main.py
```

# To run k-Nearest Neighbors


```
#!bash

bash knn/test.sh
```


To generate accuracy graph for k-NN, run


```
#!bash

python knn/graph.py

```

# To run Neural Classifier

```

#!bash

cd neural
python nn.py
python percnet.py
python lstm.py
```


# To run Recurrent Neural Predictor
```
#!bash
cd predictor
python .testAgainst.py
```
To view the feature vectors predicted, run
```

#!bash
cd predictor
python min_word_rnn.py
```
